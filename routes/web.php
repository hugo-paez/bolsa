<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index')->name('offer.index');
Route::get('/', 'HomeController@index')->name('offer.index');
Route::get('offer/create', 'HomeController@create')->name('offer.create');
Route::post('offer/', 'HomeController@store')->name('offer.store');

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::get('/register', function () {
    return view('register');
})->name('register');

Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');

  Route::group(['middleware' => 'admin'], function () {
    Route::get('home', function () {
        return view('admin.home');
    })->name('admin.home');

    Route::get('/', function () {
        return view('admin.home');
    })->name('admin.root');

    Route::resource('cities', 'AdminAuth\CitiesController', ['names' => [
      'index'   =>  'admin.cities.index',
      'create'  =>  'admin.cities.create',
      'store'   =>  'admin.cities.store',
      'show'    =>  'admin.cities.show',
      'edit'    =>  'admin.cities.edit',
      'update'  =>  'admin.cities.update',
      'destroy' =>  'admin.cities.destroy',
    ]]);
    Route::get('cities/{cities}/destroy', [
      'uses'    =>  'AdminAuth\CitiesController@destroy',
      'as'      =>  'admin.cities.destroy'
    ]);

    Route::resource('documents', 'AdminAuth\DocumentsController', ['names' => [
      'index'   =>  'admin.documents.index',
      'create'  =>  'admin.documents.create',
      'store'   =>  'admin.documents.store',
      'show'    =>  'admin.documents.show',
      'edit'    =>  'admin.documents.edit',
      'update'  =>  'admin.documents.update',
      'destroy' =>  'admin.documents.destroy',
    ]]);
    Route::get('documents/{documents}/destroy', [
      'uses'    =>  'AdminAuth\DocumentsController@destroy',
      'as'      =>  'admin.documents.destroy'
    ]);

    Route::resource('experiences', 'AdminAuth\ExperiencesController', ['names' => [
      'index'   =>  'admin.experiences.index',
      'create'  =>  'admin.experiences.create',
      'store'   =>  'admin.experiences.store',
      'show'    =>  'admin.experiences.show',
      'edit'    =>  'admin.experiences.edit',
      'update'  =>  'admin.experiences.update',
      'destroy' =>  'admin.experiences.destroy',
    ]]);
    Route::get('experiences/{users}/destroy', [
      'uses'    =>  'AdminAuth\ExperiencesController@destroy',
      'as'      =>  'admin.experiences.destroy'
    ]);

    Route::resource('majors', 'AdminAuth\MajorsController', ['names' => [
      'index'   =>  'admin.majors.index',
      'create'  =>  'admin.majors.create',
      'store'   =>  'admin.majors.store',
      'show'    =>  'admin.majors.show',
      'edit'    =>  'admin.majors.edit',
      'update'  =>  'admin.majors.update',
      'destroy' =>  'admin.majors.destroy',
    ]]);
    Route::get('majors/{majors}/destroy', [
      'uses'    =>  'AdminAuth\MajorsController@destroy',
      'as'      =>  'admin.majors.destroy'
    ]);

    Route::resource('positions', 'AdminAuth\PositionsController', ['names' => [
      'index'   =>  'admin.positions.index',
      'create'  =>  'admin.positions.create',
      'store'   =>  'admin.positions.store',
      'show'    =>  'admin.positions.show',
      'edit'    =>  'admin.positions.edit',
      'update'  =>  'admin.positions.update',
      'destroy' =>  'admin.positions.destroy',
    ]]);
    Route::get('positions/{positions}/destroy', [
      'uses'    =>  'AdminAuth\PositionsController@destroy',
      'as'      =>  'admin.positions.destroy'
    ]);

    Route::resource('regions', 'AdminAuth\RegionsController', ['names' => [
      'index'   =>  'admin.regions.index',
      'create'  =>  'admin.regions.create',
      'store'   =>  'admin.regions.store',
      'show'    =>  'admin.regions.show',
      'edit'    =>  'admin.regions.edit',
      'update'  =>  'admin.regions.update',
      'destroy' =>  'admin.regions.destroy',
    ]]);
    Route::get('regions/{regions}/destroy', [
      'uses'    =>  'AdminAuth\RegionsController@destroy',
      'as'      =>  'admin.regions.destroy'
    ]);

    Route::resource('studies', 'AdminAuth\StudiesController', ['names' => [
      'index'   =>  'admin.studies.index',
      'create'  =>  'admin.studies.create',
      'store'   =>  'admin.studies.store',
      'show'    =>  'admin.studies.show',
      'edit'    =>  'admin.studies.edit',
      'update'  =>  'admin.studies.update',
      'destroy' =>  'admin.studies.destroy',
    ]]);
    Route::get('studies/{studies}/destroy', [
      'uses'    =>  'AdminAuth\StudiesController@destroy',
      'as'      =>  'admin.studies.destroy'
    ]);

    Route::resource('durations', 'AdminAuth\DurationsController', ['names' => [
      'index'   =>  'admin.durations.index',
      'create'  =>  'admin.durations.create',
      'store'   =>  'admin.durations.store',
      'show'    =>  'admin.durations.show',
      'edit'    =>  'admin.durations.edit',
      'update'  =>  'admin.durations.update',
      'destroy' =>  'admin.durations.destroy',
    ]]);
    Route::get('durations/{durations}/destroy', [
      'uses'    =>  'AdminAuth\DurationsController@destroy',
      'as'      =>  'admin.durations.destroy'
    ]);

    Route::resource('workdays', 'AdminAuth\WorkdaysController', ['names' => [
      'index'   =>  'admin.workdays.index',
      'create'  =>  'admin.workdays.create',
      'store'   =>  'admin.workdays.store',
      'show'    =>  'admin.workdays.show',
      'edit'    =>  'admin.workdays.edit',
      'update'  =>  'admin.workdays.update',
      'destroy' =>  'admin.workdays.destroy',
    ]]);
    Route::get('workdays/{workdays}/destroy', [
      'uses'    =>  'AdminAuth\WorkdaysController@destroy',
      'as'      =>  'admin.workdays.destroy'
    ]);

    Route::resource('offers', 'AdminAuth\OffersController', ['names' => [
      'index'   =>  'admin.offers.index',
      'create'  =>  'admin.offers.create',
      'store'   =>  'admin.offers.store',
      'show'    =>  'admin.offers.show',
      'edit'    =>  'admin.offers.edit',
      'update'  =>  'admin.offers.update',
      'destroy' =>  'admin.offers.destroy',
    ]]);
    Route::get('offers/{offers}/destroy', [
      'uses'    =>  'AdminAuth\OffersController@destroy',
      'as'      =>  'admin.offers.destroy'
    ]);

    Route::resource('companies', 'AdminAuth\CompaniesController', ['names' => [
      'index'   =>  'admin.companies.index',
      'create'  =>  'admin.companies.create',
      'store'   =>  'admin.companies.store',
      'show'    =>  'admin.companies.show',
      'edit'    =>  'admin.companies.edit',
      'update'  =>  'admin.companies.update',
      'destroy' =>  'admin.companies.destroy',
    ]]);
    Route::get('companies/{companies}/destroy', [
      'uses'    =>  'AdminAuth\CompaniesController@destroy',
      'as'      =>  'admin.companies.destroy'
    ]);
    Route::get('companies/{companies}/page', [
      'uses'    =>  'AdminAuth\CompaniesController@page',
      'as'      =>  'admin.companies.page'
    ]);
    Route::post('companies/{companies}/change', [
      'uses'    =>  'AdminAuth\CompaniesController@change',
      'as'      =>  'admin.companies.change'
    ]);

    Route::resource('users', 'AdminAuth\UsersController', ['names' => [
      'index'   =>  'admin.users.index',
      'create'  =>  'admin.users.create',
      'store'   =>  'admin.users.store',
      'show'    =>  'admin.users.show',
      'edit'    =>  'admin.users.edit',
      'update'  =>  'admin.users.update',
      'destroy' =>  'admin.users.destroy',
    ]]);
    Route::get('users/{users}/destroy', [
      'uses'    =>  'AdminAuth\UsersController@destroy',
      'as'      =>  'admin.users.destroy'
    ]);
    Route::get('users/{users}/page', [
      'uses'    =>  'AdminAuth\UsersController@page',
      'as'      =>  'admin.users.page'
    ]);
    Route::post('users/{users}/change', [
      'uses'    =>  'AdminAuth\UsersController@change',
      'as'      =>  'admin.users.change'
    ]);

    Route::resource('admins', 'AdminAuth\AdminsController', ['names' => [
      'index'   =>  'admin.admins.index',
      'create'  =>  'admin.admins.create',
      'store'   =>  'admin.admins.store',
      'show'    =>  'admin.admins.show',
      'edit'    =>  'admin.admins.edit',
      'update'  =>  'admin.admins.update',
      'destroy' =>  'admin.admins.destroy',
    ]]);
    Route::get('admins/{admins}/destroy', [
      'uses'    =>  'AdminAuth\AdminsController@destroy',
      'as'      =>  'admin.admins.destroy'
    ]);
    Route::get('admins/{admins}/page', [
      'uses'    =>  'AdminAuth\AdminsController@page',
      'as'      =>  'admin.admins.page'
    ]);
    Route::post('admins/{admins}/change', [
      'uses'    =>  'AdminAuth\AdminsController@change',
      'as'      =>  'admin.admins.change'
    ]);
  });
});

Route::group(['prefix' => 'company'], function () {
  Route::get('/login', 'CompanyAuth\LoginController@showLoginForm');
  Route::post('/login', 'CompanyAuth\LoginController@login');
  Route::post('/logout', 'CompanyAuth\LoginController@logout');

  Route::get('/register', 'CompanyAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'CompanyAuth\RegisterController@register');

  Route::post('/password/email', 'CompanyAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'CompanyAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'CompanyAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'CompanyAuth\ResetPasswordController@showResetForm');

  Route::group(['middleware' => 'company'], function () {
    Route::get('/home', function () {
        return view('index');
    })->name('company.home');

    Route::get('/', function () {
        return view('index');
    })->name('company.root');

    Route::get('{company}/edit', [
      'uses'    =>  'CompanyAuth\CompaniesController@edit',
      'as'      =>  'company.companies.edit'
    ]);

    Route::put('{company}', [
      'uses'    =>  'CompanyAuth\CompaniesController@update',
      'as'      =>  'company.companies.update'
    ]);

    Route::get('{company}/page', [
      'uses'    =>  'CompanyAuth\CompaniesController@page',
      'as'      =>  'company.companies.page'
    ]);

    Route::post('{company}/change', [
      'uses'    =>  'CompanyAuth\CompaniesController@change',
      'as'      =>  'company.companies.change'
    ]);
  });
});

Route::group(['prefix' => 'user'], function () {
  Route::get('/login', 'UserAuth\LoginController@showLoginForm');
  Route::post('/login', 'UserAuth\LoginController@login');
  Route::post('/logout', 'UserAuth\LoginController@logout');

  Route::get('/register', 'UserAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'UserAuth\RegisterController@register');

  Route::post('/password/email', 'UserAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'UserAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'UserAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'UserAuth\ResetPasswordController@showResetForm');

  Route::group(['middleware' => 'user'], function () {
    Route::get('/home', function () {
        return view('index');
    })->name('user.home');

    Route::get('/', function () {
        return view('index');
    })->name('user.root');

    Route::get('{user}/edit', [
      'uses'    =>  'UserAuth\UsersController@edit',
      'as'      =>  'user.users.edit'
    ]);

    Route::put('{user}', [
      'uses'    =>  'UserAuth\UsersController@update',
      'as'      =>  'user.users.update'
    ]);

    Route::get('{user}/page', [
      'uses'    =>  'UserAuth\UsersController@page',
      'as'      =>  'user.users.page'
    ]);

    Route::post('{user}/change', [
      'uses'    =>  'UserAuth\UsersController@change',
      'as'      =>  'user.users.change'
    ]);
  });
});
