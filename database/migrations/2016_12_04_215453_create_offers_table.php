<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('offers', function (Blueprint $table) {
      $table->increments('id');
      $table->text('title');
      $table->string('description');
      $table->date('date');
      $table->string('requirement');
      $table->char('vacancy', 3);
      $table->string('position');
      $table->boolean('status');
      $table->integer('company_id')->unsigned();
      $table->integer('duration_id')->unsigned();
      $table->integer('workday_id')->unsigned();
      $table->integer('experience_id')->unsigned();
      $table->integer('position_id')->unsigned();
      $table->integer('study_id')->unsigned();
      $table->integer('city_id')->unsigned();
      $table->timestamps();

      $table->foreign('company_id')->references('id')->on('companies')->OnDelete('cascade');
      $table->foreign('duration_id')->references('id')->on('durations')->OnDelete('cascade');
      $table->foreign('workday_id')->references('id')->on('workdays')->OnDelete('cascade');
      $table->foreign('city_id')->references('id')->on('cities')->OnDelete('cascade');
      $table->foreign('experience_id')->references('id')->on('experiences')->OnDelete('cascade');
      $table->foreign('position_id')->references('id')->on('positions')->OnDelete('cascade');
      $table->foreign('study_id')->references('id')->on('studies')->OnDelete('cascade');
    });

    Schema::create('offers_majors', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('offer_id')->unsigned();
      $table->integer('major_id')->unsigned();
      $table->timestamps();

      $table->foreign('offer_id')->references('id')->on('offers')->OnDelete('cascade');
      $table->foreign('major_id')->references('id')->on('majors')->OnDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('offers_majors');
    Schema::dropIfExists('offers');
  }
}
