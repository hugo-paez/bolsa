<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('users', function (Blueprint $table) {
        $table->increments('id');
        $table->string('rut', 10)->unique();
        $table->string('first_name', 50);
        $table->string('last_name', 50);
        $table->string('email')->unique();
        $table->integer('major_id')->unsigned();
        $table->integer('city_id')->unsigned();
        $table->string('photo')->nullable();;
        $table->string('telephone', 9);
        $table->string('address');
        $table->string('password', 60);
        $table->rememberToken();
        $table->timestamps();

        $table->foreign('major_id')->references('id')->on('majors')->OnDelete('cascade');
        $table->foreign('city_id')->references('id')->on('cities')->OnDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('users');
    }
}
