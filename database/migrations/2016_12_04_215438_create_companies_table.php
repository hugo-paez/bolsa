<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('companies', function (Blueprint $table) {
        $table->increments('id');
        $table->string('rut', 10)->unique();
        $table->string('name');
        $table->string('email')->unique();
        $table->string('activity');
        $table->string('web_page');
        $table->string('photo')->nullable();;
        $table->string('address');
        $table->integer('city_id')->unsigned();
        $table->string('password');
        $table->rememberToken();
        $table->timestamps();

        $table->foreign('city_id')->references('id')->on('cities')->OnDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('companies');
    }
}
