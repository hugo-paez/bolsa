@extends('template.guest')

@section('title', trans('guest.list_offers'))


@section('content')
  <a class="btn btn-success" href="{!! url('/user/register') !!}" role="button">{!! trans('auth.register_user') !!}</a>
  <a class="btn btn-info pull-right" href="{!! url('/company/register') !!}" role="button">{!! trans('auth.register_company') !!}</a>
@endsection
