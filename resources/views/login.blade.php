@extends('template.guest')

@section('title', trans('guest.list_offers'))


@section('content')
  <a class="btn btn-success" href="{!! url('/user/login') !!}" role="button">{!! trans('auth.login_user') !!}</a>
  <a class="btn btn-info pull-right" href="{!! url('/company/login') !!}" role="button">{!! trans('auth.login_company') !!}</a>
@endsection
