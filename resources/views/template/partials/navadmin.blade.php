<div class="container">
  <div class="navbar-header">
    <!-- Collapsed Hamburger -->
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
      <span class="sr-only">Toggle Navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <!-- Branding Image -->
    <a class="navbar-brand" href="{{ url('/admin') }}">
      Panel de Administración
    </a>
  </div>

  <div class="collapse navbar-collapse" id="app-navbar-collapse">
    <!-- Left Side Of Navbar -->
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
          {!! trans('admin.maintainer') !!} <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="{!! route('admin.regions.index') !!}">{!! trans('admin.regions') !!}</a></li>
          <li><a href="{!! route('admin.cities.index') !!}">{!! trans('admin.cities') !!}</a></li>
          <li role="separator" class="divider"></li>
          <li><a href="{!! route('admin.studies.index') !!}">{!! trans('admin.studies') !!}</a></li>
          <li><a href="{!! route('admin.workdays.index') !!}">{!! trans('admin.workdays') !!}</a></li>
          <li><a href="{!! route('admin.majors.index') !!}">{!! trans('admin.majors') !!}</a></li>
          <li><a href="{!! route('admin.positions.index') !!}">{!! trans('admin.positions') !!}</a></li>
          <li><a href="{!! route('admin.durations.index') !!}">{!! trans('admin.durations') !!}</a></li>
          <li><a href="{!! route('admin.experiences.index') !!}">{!! trans('admin.experiences') !!}</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
          {!! trans('admin.transactional') !!} <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="{!! route('admin.users.index') !!}">{!! trans('admin.users') !!}</a></li>
          <li><a href="{!! route('admin.companies.index') !!}">{!! trans('admin.companies') !!}</a></li>
          <li><a href="{!! route('admin.admins.index') !!}">{!! trans('admin.admins') !!}</a></li>
          <li><a href="{!! route('admin.offers.index') !!}">{!! trans('admin.offers') !!}</a></li>
        </ul>
      </li>
    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="nav navbar-nav navbar-right">
      <!-- Authentication Links -->
      @if (Auth::guard('admin')->guest())
        <li><a href="{{ url('/admin/login') }}">{!! trans('auth.login') !!}</a></li>
      @else
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {{ Auth::guard('admin')->user()->fullname }} <span class="caret"></span>
          </a>

          <ul class="dropdown-menu" role="menu">
            <li><a href="{!! route('admin.admins.edit', Auth::guard('admin')->user()->id ) !!}">{!! trans('admin.edit_profile') !!}</li>
            <li><a href="{!! route('admin.admins.page', Auth::guard('admin')->user()->id ) !!}">{!! trans('admin.change_password') !!}</li>
            <li>
              <a href="{{ url('/admin/logout') }}"
                  onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                  {!! trans('auth.logout') !!}
              </a>

              <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
          </ul>
        </li>
      @endif
    </ul>
  </div>
</div>
