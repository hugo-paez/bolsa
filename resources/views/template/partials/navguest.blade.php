<div class="container">
  <div class="navbar-header">
    <!-- Collapsed Hamburger -->
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
      <span class="sr-only">Toggle Navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <!-- Branding Image -->
    <a class="navbar-brand" href="{{ url('/user') }}">
      {{ config('app.name', 'Laravel') }}
    </a>
  </div>

  <div class="collapse navbar-collapse" id="app-navbar-collapse">
    <!-- Left Side Of Navbar -->
    <ul class="nav navbar-nav">
      &nbsp;
    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="nav navbar-nav navbar-right">
      <!-- Authentication Links -->
      @if (Auth::guard('user')->guest() && Auth::guard('company')->guest())
        <li><a href="{{ url('/login') }}">{!! trans('auth.login') !!}</a></li>
        <li><a href="{{ url('/register') }}">{!! trans('auth.register') !!}</a></li>
      @else
        @if(Auth::guard('user')->user())
          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::guard('user')->user()->fullname }} <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" role="menu">
                <li><a href="{!! route('user.users.edit', Auth::guard('user')->user()->id ) !!}">{!! trans('admin.edit_profile') !!}</li>
                <li><a href="{!! route('user.users.page', Auth::guard('user')->user()->id ) !!}">{!! trans('admin.change_password') !!}</li>
                <li>
                    <a href="{{ url('/user/logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        {!! trans('auth.logout') !!}
                    </a>

                    <form id="logout-form" action="{{ url('/user/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                </li>
              </ul>
          </li>
        @else
          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::guard('company')->user()->name }} <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" role="menu">
                <li><a href="{!! route('company.companies.edit', Auth::guard('company')->user()->id ) !!}">{!! trans('admin.edit_profile') !!}</li>
                <li><a href="{!! route('company.companies.page', Auth::guard('company')->user()->id ) !!}">{!! trans('admin.change_password') !!}</li>
                <li>
                    <a href="{{ url('/company/logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        {!! trans('auth.logout') !!}
                    </a>

                    <form id="logout-form" action="{{ url('/company/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                </li>
              </ul>
          </li>
        @endif
      @endif
    </ul>
  </div>
</div>
