<div class="container">
  <div class="navbar-header">
    <!-- Collapsed Hamburger -->
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
      <span class="sr-only">Toggle Navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <!-- Branding Image -->
    <a class="navbar-brand" href="{{ url('/user') }}">
      {{ config('app.name', 'Laravel') }}: Panel de Usuario
    </a>
  </div>

  <div class="collapse navbar-collapse" id="app-navbar-collapse">
    <!-- Left Side Of Navbar -->
    <ul class="nav navbar-nav">
      &nbsp;
    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="nav navbar-nav navbar-right">
      <!-- Authentication Links -->
      @if (Auth::guard('user')->guest())
        <li><a href="{{ url('/user/login') }}">{!! trans('auth.login') !!}</a></li>
        <li><a href="{{ url('/user/register') }}">{!! trans('auth.register') !!}</a></li>
      @else
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {{ Auth::guard('user')->user()->fullname }} <span class="caret"></span>
          </a>

          <ul class="dropdown-menu" role="menu">
            <li><a href="{!! route('user.users.edit', Auth::guard('user')->user()->id ) !!}">{!! trans('admin.edit_profile') !!}</li>
            <li><a href="{!! route('user.users.page', Auth::guard('user')->user()->id ) !!}">{!! trans('admin.change_password') !!}</li>
            <li>
              <a href="{{ url('/user/logout') }}"
                  onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                  {!! trans('auth.logout') !!}
              </a>

              <form id="logout-form" action="{{ url('/user/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
          </ul>
        </li>
      @endif
    </ul>
  </div>
</div>
