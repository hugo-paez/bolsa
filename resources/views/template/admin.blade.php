<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="siomara, plataforma, web, complejo, deportivo, serena, coquimbo, region" />
    <meta name="author" content="Jorge Varela" />
    <meta name="copyright" content="siomara.cl" />
    <meta name="robots" content="index, follow" />

    <!-- Facebook -->
    <meta property="og:locale" content="es_ES">
    <meta property="og:type" content="Plataforma">
    <meta property="og:title" content="Complejo Deportivo Siomara">
    <meta property="og:description" content="Esta es la pagina del complejo deportivo de siomara">
    <meta property="og:url" content="http://admin.siomara.cl/">
    <meta property="og:site_name" content="Complejo Deportivo Siomara">
    <meta property="og:image" content="link de la imagen a mostrar">

    <!-- Twitter -->
    <meta name="twitter:card" content="Siomara" />
    <meta name="twitter:description" content="Esta es la pagina del complejo deportivo de siomara" />
    <meta name="twitter:site" content="@SIOMARA" />
    <meta name="twitter:title" content="Complejo Deportivo Siomara" />
    <meta name="twitter:url" content="http://admin.siomara.cl/" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Default') | Panel de Administración</title>

    <!-- Fonts -->
    {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css') !!}
    {!! Html::style('https://fonts.googleapis.com/css?family=Lato:100,300,400,700') !!}

    <!-- Styles -->
    {!! Html::style('css/normalize.min.css') !!}
    {!! Html::style('plugins/bootstrap/css/bootstrap.min.css') !!}
    @yield('css')
  </head>
  <body id="app-layout">
    <!-- Nav bar -->
    <nav class="navbar navbar-default navbar-static-top">
      @include('template.partials.navadmin')
    </nav>

    <!-- Container -->
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading">@yield('title', 'Default')</div>
            <div class="panel-body">
              @include('template.partials.errors')
              @include('flash::message')
              @yield('content')
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Footer -->
    <footer class="admin-footer">
      @include('template.partials.footeradmin')
    </footer>

    <!-- JavaScripts -->
    {!! Html::script('plugins/jquery/js/jquery-2.2.3.min.js') !!}
    {!! Html::script('plugins/bootstrap/js/bootstrap.min.js') !!}
    <script>
        window.Laravel = {{ json_encode([
            'csrfToken' => csrf_token(),
        ]) }}
    </script>
    @yield('javascript')
  </body>
</html>
