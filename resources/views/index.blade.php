@extends('template.guest')

@section('title', trans('guest.list_offers'))

@section('content')
  <!-- Search -->
  {!! Form::model(Request::all(), ['route' => 'offer.index', 'method' => 'GET', 'class' => 'navbar-form']) !!}
    <div class='navbar-left'>
      <div class='input-group'>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('guest.search_offer'), 'aria-describedby' => 'search']) !!}
        <span class="input-group-btn">
          {!! Form::button("<span class='glyphicon glyphicon-search' aria-hidden='true'>", array('class' => 'btn btn-search', 'type' => 'submit')) !!}
        </span>
      </div>
    </div>

    <div class='navbar-right'>
      <div class="input-group">
        @if(Auth::guard('company')->user())
          <a href="{!! route('offer.create') !!}" class="btn btn-info">{!! trans('guest.insert_offer') !!}</a>
        @endif
      </div>
    </div>
  {!! Form::close() !!}
  <br>
  <hr>

  <!-- Content -->
  <div class="table-responsive">
    <table class="table table-hover">
      <thead>
        <th>{!! trans('guest.id') !!}</th>
        <th>{!! trans('guest.name') !!}</th>
      </thead>
      <tbody>
        @foreach($offers as $offer)
          <tr>
            <td>{!! $offer->title !!}</td>
            <td>{!! $offer->description !!}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="text-center">
      {!! $offers->appends(Request::all())->render() !!}
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('guest.close') !!}</button>
          <a class="btn btn-danger" href="" id="link">{!! trans('guest.delete') !!}</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('javascript')
  <script type="text/javascript">
    $('#modalDelete').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var name = button.data('name');
      var id = button.data('id');

      var modal = $(this);
      modal.find('.modal-title').text("{!! trans('guest.delete_title', ['name' => '" + name + "']) !!}");
      modal.find('.modal-body').text("{!! trans('guest.delete_text', ['name' => '" + name + "', 'type' => 'la región de']) !!}");

      var url = "/guest/offers/" + id + "/destroy";

      $("#link").attr("href", url);
    });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
      $('[data-toggle="modal"]').tooltip()
    })
  </script>
@endsection
