@extends('template.user')

@section('title', trans('auth.register'))

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  {!! Form::open(['url' => '/user/register', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('rut') ? ' has-error' : '' }}">
      {!! Form::label('rut', trans('auth.rut'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::text('rut', old('rut'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_rut'), 'required']) !!}

        @if ($errors->has('rut'))
          <span class="help-block">
            <strong>{{ $errors->first('rut') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
      {!! Form::label('first_name', trans('auth.first_name'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_first_name'), 'required']) !!}

        @if ($errors->has('first_name'))
          <span class="help-block">
            <strong>{{ $errors->first('first_name') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
      {!! Form::label('last_name', trans('auth.last_name'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_last_name'), 'required']) !!}

        @if ($errors->has('last_name'))
          <span class="help-block">
            <strong>{{ $errors->first('last_name') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      {!! Form::label('email', trans('auth.email'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => trans('auth.ph_email'), 'required']) !!}

        @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
      {!! Form::label('telephone', trans('auth.telephone'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::text('telephone', old('telephone'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_telephone'), 'required']) !!}

        @if ($errors->has('telephone'))
          <span class="help-block">
            <strong>{{ $errors->first('telephone') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
      {!! Form::label('city_id', trans('auth.city'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::select('city_id', $cities, null, ['class' => 'form-control', 'placeholder' => trans('auth.ph_city'), 'required']) !!}

        @if ($errors->has('city_id'))
          <span class="help-block">
            <strong>{{ $errors->first('city_id') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('major_id') ? ' has-error' : '' }}">
      {!! Form::label('major_id', trans('auth.major'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::select('major_id', $majors, null, ['class' => 'form-control', 'placeholder' => trans('auth.ph_major'), 'required']) !!}

        @if ($errors->has('area_id'))
          <span class="help-block">
            <strong>{{ $errors->first('major_id') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
      {!! Form::label('address', trans('auth.address'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_address'), 'required']) !!}

        @if ($errors->has('address'))
          <span class="help-block">
            <strong>{{ $errors->first('address') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
      {!! Form::label('password', trans('auth.password'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('auth.ph_password'), 'required']) !!}

        @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
      {!! Form::label('password_confirmation', trans('auth.password_confirmation'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
       {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('auth.ph_password'), 'required']) !!}

        @if ($errors->has('password_confirmation'))
          <span class="help-block">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col-md-6 col-md-offset-4">
        {!! Form::button("<i class='fa fa-btn fa-user'></i> " . trans('auth.register'), array('class' => 'btn btn-primary', 'type' => 'submit')) !!}
      </div>
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/select2/js/select2.min.js') !!}
  {!! Html::script('plugins/rut/js/jquery.Rut.min.js') !!}
  <script type="text/javascript">
    $(document).ready(function() {
      $("#city_id").select2({
        placeholder: "{!! trans('auth.ph_city') !!}",
        allowClear: true
      });

      $("#major_id").select2({
        placeholder: "{!! trans('auth.ph_major') !!}",
        allowClear: true
      });

      $('#rut').Rut({
        on_error: function(){ alert('Rut incorrecto'); },
        format_on: 'keyup'
      });
    });
  </script>
@endsection
