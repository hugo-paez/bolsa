@extends('template.user')

@section('title', trans('user.edit_user') . ' ' . $user->fullname)

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['user.users.update', $user], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('first_name', trans('user.name'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('first_name', $user->first_name, ['class' => 'form-control', 'placeholder' => trans('user.ph_first_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('last_name', trans('user.last_name'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('last_name', $user->last_name, ['class' => 'form-control', 'placeholder' => trans('user.ph_last_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('email', trans('user.email'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::email('email', $user->email, ['class' => 'form-control', 'placeholder' => trans('user.ph_email'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('telephone', trans('user.telephone'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('telephone', $user->telephone, ['class' => 'form-control', 'placeholder' => trans('user.ph_telephone')]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('major_id', trans('user.major'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::select('major_id', $majors, $user->major_id, ['class' => 'form-control', 'placeholder' => trans('user.ph_major'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('city_id', trans('user.city'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::select('city_id', $cities, $user->city_id, ['class' => 'form-control', 'placeholder' => trans('user.ph_city'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('address', trans('user.address'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('address', $user->address, ['class' => 'form-control', 'placeholder' => trans('user.ph_address_user'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('user.edit_user'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/select2/js/select2.min.js') !!}
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $("#city_id").select2({
        placeholder: "{!! trans('user.ph_city') !!}",
        allowClear: true
      });

      $("#major_id").select2({
        placeholder: "{!! trans('user.ph_major') !!}",
        allowClear: true
      });

      $("#type").select2({
        placeholder: "{!! trans('user.ph_type') !!}",
        allowClear: true
      });
    });
  </script>
@endsection
