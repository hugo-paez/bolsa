@extends('template.user')

@section('title', trans('user.change_user') . ' a ' . $user->fullname)

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['user.users.change', $user->id], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('old_password', trans('user.old_password'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::password('old_password', ['class' => 'form-control', 'placeholder' => trans('user.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('new_password', trans('user.new_password'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::password('new_password', ['class' => 'form-control', 'placeholder' => trans('user.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('new_password_confirmation', trans('user.new_password_confirmation'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::password('new_password_confirmation', ['class' => 'form-control', 'placeholder' => trans('user.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('user.insert_user'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
