@extends('template.company')

@section('title', trans('company.change_company') . ' a ' . $company->name)

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['company.companies.change', $company->id], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('old_password', trans('company.old_password'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::password('old_password', ['class' => 'form-control', 'placeholder' => trans('company.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('new_password', trans('company.new_password'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::password('new_password', ['class' => 'form-control', 'placeholder' => trans('company.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('new_password_confirmation', trans('company.new_password_confirmation'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::password('new_password_confirmation', ['class' => 'form-control', 'placeholder' => trans('company.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('company.insert_company'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
