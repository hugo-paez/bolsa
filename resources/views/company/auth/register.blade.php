@extends('template.company')

@section('title', trans('auth.register'))

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  {!! Form::open(['url' => '/company/register', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('rut') ? ' has-error' : '' }}">
      {!! Form::label('rut', trans('auth.rut'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::text('rut', old('rut'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_rut'), 'required']) !!}

        @if ($errors->has('rut'))
          <span class="help-block">
            <strong>{{ $errors->first('rut') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      {!! Form::label('name', trans('auth.name'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_name'), 'required']) !!}

        @if ($errors->has('name'))
          <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      {!! Form::label('email', trans('auth.email'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => trans('auth.ph_email'), 'required']) !!}

        @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('web_page') ? ' has-error' : '' }}">
      {!! Form::label('web_page', trans('auth.web_page'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::url('web_page', old('web_page'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_web_page'), 'required']) !!}

        @if ($errors->has('web_page'))
          <span class="help-block">
            <strong>{{ $errors->first('web_page') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
      {!! Form::label('city_id', trans('auth.city'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::select('city_id', $cities, null, ['class' => 'form-control', 'placeholder' => trans('auth.ph_city'), 'required']) !!}

        @if ($errors->has('city_id'))
          <span class="help-block">
            <strong>{{ $errors->first('city_id') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('activity') ? ' has-error' : '' }}">
      {!! Form::label('activity', trans('auth.activity'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::text('activity', old('activity'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_activity'), 'required']) !!}

        @if ($errors->has('activity'))
          <span class="help-block">
            <strong>{{ $errors->first('activity') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
      {!! Form::label('address', trans('auth.address'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_address'), 'required']) !!}

        @if ($errors->has('address'))
          <span class="help-block">
            <strong>{{ $errors->first('address') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
      {!! Form::label('password', trans('auth.password'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('auth.ph_password'), 'required']) !!}

        @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
      {!! Form::label('password_confirmation', trans('auth.password_confirmation'), ['class' => 'col-sm-4 control-label']) !!}

      <div class="col-md-6">
       {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('auth.ph_password'), 'required']) !!}

        @if ($errors->has('password_confirmation'))
          <span class="help-block">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col-md-6 col-md-offset-4">
        {!! Form::button("<i class='fa fa-btn fa-user'></i> " . trans('auth.register'), array('class' => 'btn btn-primary', 'type' => 'submit')) !!}
      </div>
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/select2/js/select2.min.js') !!}
  {!! Html::script('plugins/rut/js/jquery.Rut.min.js') !!}
  <script type="text/javascript">
    $(document).ready(function() {
      $("#city_id").select2({
        placeholder: "{!! trans('auth.ph_city') !!}",
        allowClear: true
      });

      $('#rut').Rut({
        on_error: function(){ alert('Rut incorrecto'); },
        format_on: 'keyup'
      });
    });
  </script>
@endsection
