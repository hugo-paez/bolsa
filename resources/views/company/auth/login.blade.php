@extends('template.company')

@section('title', trans('auth.login'))

@section('content')
  {!! Form::open(['url' => '/company/login', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('rut') ? ' has-error' : '' }}">
      {!! Form::label('rut', trans('auth.rut'), ['class' => 'col-md-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::text('rut', old('rut'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_rut'), 'required']) !!}

        @if ($errors->has('rut'))
          <span class="help-block">
            <strong>{{ $errors->first('rut') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
      {!! Form::label('password', trans('auth.password'), ['class' => 'col-md-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('auth.ph_password'), 'required']) !!}

        @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember"> {{ trans('auth.remember') }}
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
      <div class="col-md-6 col-md-offset-4">
        {!! Form::button("<i class='fa fa-btn sign-in'></i> " . trans('auth.login'), array('class' => 'btn btn-primary', 'type' => 'submit')) !!}

        <a class="btn btn-link" href="{{ url('/password/reset') }}">{{ trans('auth.forgot') }}</a>
      </div>
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/rut/js/jquery.Rut.min.js') !!}
  <script type="text/javascript">
    $(document).ready(function() {
      $('#rut').Rut({
        on_error: function(){ alert('Rut incorrecto'); },
        format_on: 'keyup'
      });
    });
  </script>
@endsection
