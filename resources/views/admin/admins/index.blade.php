@extends('template.admin')

@section('title', trans('admin.list_admins'))

@section('content')
  <!-- Search -->
  {!! Form::model(Request::all(), ['route' => 'admin.admins.index', 'method' => 'GET', 'class' => 'navbar-form']) !!}
    <div class='navbar-left'>
      <div class='input-group'>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('admin.search_admin'), 'aria-describedby' => 'search']) !!}
        <span class="input-group-btn">
          {!! Form::button("<span class='glyphicon glyphicon-search' aria-hidden='true'>", array('class' => 'btn btn-search', 'type' => 'submit')) !!}
        </span>
      </div>
    </div>

    <div class='navbar-right'>
      <div class="input-group">
        <a href="{!! route('admin.admins.create') !!}" class="btn btn-info">{!! trans('admin.insert_admin') !!}</a>
      </div>
    </div>
  {!! Form::close() !!}
  <br>
  <hr>

  <!-- Content -->
  <div class="table-responsive">
    <table class="table table-hover">
      <thead>
        <th>{!! trans('admin.id') !!}</th>
        <th>{!! trans('admin.name') !!}</th>
        <th>{!! trans('admin.email') !!}</th>
        <th>{!! trans('admin.action') !!}</th>
      </thead>
      <tbody>
        @foreach($admins as $admin)
          <tr>
            <td>{!! $admin->id !!}</td>
            <td>{!! $admin->fullname !!}</td>
            <td>{!! $admin->email !!}</td>
            <td>
              <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {!! trans('admin.action') !!} <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="{!! route('admin.admins.show', $admin->id) !!}">{!! trans('admin.show_admins') !!}</a></li>
                  <li><a href="{!! route('admin.admins.edit', $admin->id) !!}">{!! trans('admin.edit_admins') !!}</a></li>
                  <li><a href="{!! route('admin.admins.destroy', $admin->id) !!}">{!! trans('admin.destroy_admins') !!}</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="{!! route('admin.admins.page', $admin->id ) !!}">{!! trans('admin.change_password') !!}</a></li>
                </ul>
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="text-center">
      {!! $admins->appends(Request::all())->render() !!}
    </div>
  </div>
@endsection
