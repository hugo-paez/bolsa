@extends('template.admin')

@section('title', trans('admin.edit_admin') . ' ' . $admin->fullname)

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['admin.admins.update', $admin], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('first_name', trans('admin.name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('first_name', $admin->first_name, ['class' => 'form-control', 'placeholder' => trans('admin.ph_first_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('last_name', trans('admin.last_name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('last_name', $admin->last_name, ['class' => 'form-control', 'placeholder' => trans('admin.ph_last_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('email', trans('admin.email'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::email('email', $admin->email, ['class' => 'form-control', 'placeholder' => trans('admin.ph_email'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.edit_admin'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
