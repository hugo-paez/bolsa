@extends('template.admin')

@section('title', trans('admin.change_admin') . ' a ' . $admin->fullname)

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['admin.admins.change', $admin->id], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
      {!! Form::label('old_password', trans('admin.old_password'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::password('old_password', ['class' => 'form-control', 'placeholder' => trans('admin.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('new_password', trans('admin.new_password'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::password('new_password', ['class' => 'form-control', 'placeholder' => trans('admin.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('new_password_confirmation', trans('admin.new_password_confirmation'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::password('new_password_confirmation', ['class' => 'form-control', 'placeholder' => trans('admin.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.insert_admin'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
