@extends('template.admin')

@section('title', trans('admin.insert_offer'))

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => 'admin.offers.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('title', trans('admin.title'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_title'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('description', trans('admin.description'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_description'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('requirement', trans('admin.requirement'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('requirement', null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_requirement'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('vacancy', trans('admin.vacancy'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('vacancy', null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_vacancy'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('company_id', trans('admin.company'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('company_id', $companies, null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_company'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('duration_id', trans('admin.duration'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('duration_id', $durations, null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_duration'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('workday_id', trans('admin.workday'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('workday_id', $workdays, null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_workday'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('experience_id', trans('admin.experience'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('experience_id', $experiences, null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_experience'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('position_id', trans('admin.position'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('position_id', $positions, null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_position'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('city_id', trans('admin.cities'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('city_id', $cities, null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_city'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('study_id', trans('admin.study'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('study_id', $studies, null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_study'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.insert_offer'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/select2/js/select2.min.js') !!}
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $("#city_id").select2({
        placeholder: "{!! trans('admin.ph_city') !!}",
        allowClear: true
      });

      $("#study_id").select2({
        placeholder: "{!! trans('admin.ph_study') !!}",
        allowClear: true
      });

      $("#position_id").select2({
        placeholder: "{!! trans('admin.ph_position') !!}",
        allowClear: true
      });

      $("#experience_id").select2({
        placeholder: "{!! trans('admin.ph_experience') !!}",
        allowClear: true
      });

      $("#workday_id").select2({
        placeholder: "{!! trans('admin.ph_workday') !!}",
        allowClear: true
      });

      $("#duration_id").select2({
        placeholder: "{!! trans('admin.ph_duration') !!}",
        allowClear: true
      });

      $("#company_id").select2({
        placeholder: "{!! trans('admin.ph_company') !!}",
        allowClear: true
      });
    });
  </script>
@endsection
