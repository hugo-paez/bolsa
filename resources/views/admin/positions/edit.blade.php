@extends('template.admin')

@section('title', trans('admin.edit_position') . ' ' . $position->name)

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['admin.positions.update', $position], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('name', trans('admin.name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('name', $position->name, ['class' => 'form-control', 'placeholder' => trans('admin.ph_name_position'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.edit_position'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
