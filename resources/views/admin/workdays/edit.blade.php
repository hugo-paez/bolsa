@extends('template.admin')

@section('title', trans('admin.edit_workday') . ' ' . $workday->name)

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['admin.workdays.update', $workday], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('name', trans('admin.name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('name', $workday->name, ['class' => 'form-control', 'placeholder' => trans('admin.ph_name_workday'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.edit_workday'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
