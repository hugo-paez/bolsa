@extends('template.admin')

@section('title', trans('admin.list_workdays'))

@section('content')
  <!-- Search -->
  {!! Form::model(Request::all(), ['route' => 'admin.workdays.index', 'method' => 'GET', 'class' => 'navbar-form']) !!}
    <div class='navbar-left'>
      <div class='input-group'>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('admin.search_workday'), 'aria-describedby' => 'search']) !!}
        <span class="input-group-btn">
          {!! Form::button("<span class='glyphicon glyphicon-search' aria-hidden='true'>", array('class' => 'btn btn-search', 'type' => 'submit')) !!}
        </span>
      </div>
    </div>

    <div class='navbar-right'>
      <div class="input-group">
        <a href="{!! route('admin.workdays.create') !!}" class="btn btn-info">{!! trans('admin.insert_workday') !!}</a>
      </div>
    </div>
  {!! Form::close() !!}
  <br>
  <hr>

  <!-- Content -->
  <div class="table-responsive">
    <table class="table table-hover">
      <thead>
        <th>{!! trans('admin.id') !!}</th>
        <th>{!! trans('admin.name') !!}</th>
        <th>{!! trans('admin.action') !!}</th>
      </thead>
      <tbody>
        @foreach($workdays as $workday)
          <tr>
            <td>{!! $workday->id !!}</td>
            <td>{!! $workday->name !!}</td>
            <td>
              <a href="{!! route('admin.workdays.edit', $workday->id) !!}" class="btn btn-warning" data-toggle="tooltip" title="{!! trans('admin.tt_edit', ['name' => $workday->name]) !!}"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
              <a href="" class="btn btn-danger" data-toggle="modal" data-target="#modalDelete" data-name="{!! $workday->name !!}" data-id="{!! $workday->id !!}" title="{!! trans('admin.tt_delete', ['name' => $workday->name]) !!}"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="text-center">
      {!! $workdays->appends(Request::all())->render() !!}
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('admin.close') !!}</button>
          <a class="btn btn-danger" href="" id="link">{!! trans('admin.delete') !!}</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('javascript')
  <script type="text/javascript">
    $('#modalDelete').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var name = button.data('name');
      var id = button.data('id');

      var modal = $(this);
      modal.find('.modal-title').text("{!! trans('admin.delete_title', ['name' => '" + name + "']) !!}");
      modal.find('.modal-body').text("{!! trans('admin.delete_text', ['name' => '" + name + "', 'type' => 'la región de']) !!}");

      var url = "/admin/workdays/" + id + "/destroy";

      $("#link").attr("href", url);
    });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
      $('[data-toggle="modal"]').tooltip()
    })
  </script>
@endsection
