@extends('template.admin')

@section('title', trans('auth.login'))

@section('content')
  {!! Form::open(['url' => '/admin/login', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      {!! Form::label('email', trans('auth.email'), ['class' => 'col-md-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => trans('auth.ph_email'), 'required']) !!}

        @if ($errors->has('rut'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
      {!! Form::label('password', trans('auth.password'), ['class' => 'col-md-4 control-label']) !!}

      <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('auth.ph_password'), 'required']) !!}

        @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember"> {{ trans('auth.remember') }}
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
      <div class="col-md-6 col-md-offset-4">
        {!! Form::button("<i class='fa fa-btn sign-in'></i> " . trans('auth.login'), array('class' => 'btn btn-primary', 'type' => 'submit')) !!}

        <a class="btn btn-link" href="{{ url('/admin/password/reset') }}">{{ trans('auth.forgot') }}</a>
      </div>
    </div>
  {!! Form::close() !!}
@endsection
