@extends('template.admin')

@section('title', trans('admin.insert_city'))

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => 'admin.cities.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('name', trans('admin.name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_name_city'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('region_id', trans('admin.region'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('region_id', $regions, old('region_id'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_region'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.insert_city'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/select2/js/select2.min.js') !!}
  <script type="text/javascript">
    $(document).ready(function() {
    	$("#region_id").select2({
        placeholder: "{!! trans('admin.ph_region') !!}",
        allowClear: true
      });
    });
  </script>
@endsection
