@extends('template.admin')

@section('title', trans('admin.list_companies'))

@section('css')
  {!! Html::style('css/dropdown.css') !!}
@endsection

@section('content')
  <!-- Search -->
  {!! Form::model(Request::all(), ['route' => 'admin.companies.index', 'method' => 'GET', 'class' => 'navbar-form']) !!}
    <div class='navbar-left'>
      <div class='input-group'>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('admin.search_company'), 'aria-describedby' => 'search']) !!}
        <span class="input-group-btn">
          {!! Form::button("<span class='glyphicon glyphicon-search' aria-hidden='true'>", array('class' => 'btn btn-search', 'type' => 'submit')) !!}
        </span>
      </div>
    </div>

    <div class='navbar-right'>
      <div class="input-group">
        <a href="{!! route('admin.companies.create') !!}" class="btn btn-info">{!! trans('admin.insert_company') !!}</a>
      </div>
    </div>
  {!! Form::close() !!}
  <br>
  <hr>

  <!-- Content -->
  <div class="table-responsive">
    <table class="table table-hover">
      <thead>
        <th>{!! trans('admin.id') !!}</th>
        <th>{!! trans('admin.name') !!}</th>
        <th>{!! trans('admin.position') !!}</th>
        <th>{!! trans('admin.city') !!}</th>
        <th>{!! trans('admin.address') !!}</th>
        <th>{!! trans('admin.action') !!}</th>
      </thead>
      <tbody>
        @foreach($companies as $company)
          <tr>
            <td>{!! $company->id !!}</td>
            <td>{!! $company->name !!}</td>
            <td>{!! $company->typecompany !!}</td>
            <td>{!! $company->city->name !!}</td>
            <td>{!! $company->address !!}</td>
            <td>
              <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {!! trans('admin.action') !!} <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="{!! route('admin.companies.show', $company->id) !!}">{!! trans('admin.show_companies') !!}</a></li>
                  <li><a href="{!! route('admin.companies.edit', $company->id) !!}">{!! trans('admin.edit_companies') !!}</a></li>
                  <li><a href="{!! route('admin.companies.destroy', $company->id) !!}">{!! trans('admin.destroy_companies') !!}</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="{!! route('admin.companies.page', $company->id ) !!}">{!! trans('admin.change_password') !!}</a></li>
                </ul>
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="text-center">
      {!! $companies->appends(Request::all())->render() !!}
    </div>
  </div>
@endsection
