@extends('template.main')

@section('title', trans('admin.show_company') . ' ' . $company->fullname)

@section('content')
  <div>
    <ul>
      <li><b>{{ trans('admin.rut') }}:</b> {{ $company->rut }}</li>
      <li><b>{{ trans('admin.name') }}:</b> {{ $company->fullname }}</li>
      <li><b>{{ trans('admin.position') }}:</b> {{ $company->typecompany }}</li>
      <li><b>{{ trans('admin.email') }}:</b> {{ $company->email }}</li>
      <li><b>{{ trans('admin.telephone') }}:</b> {{ $company->telephone }}</li>
      <li><b>{{ trans('admin.city') }}:</b> {{ $company->city->name }}</li>
      <li><b>{{ trans('admin.address') }}:</b> {{ $company->address }}</li>
    </ul>
  </div>
@endsection
