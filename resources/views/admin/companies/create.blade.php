@extends('template.admin')

@section('title', trans('admin.insert_company'))

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => 'admin.admin.companies.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('rut', trans('admin.rut'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('rut', old('rut'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_rut'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('name', trans('admin.name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('email', trans('admin.email'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_email'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('activity', trans('admin.activity'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('activity', old('activity'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_activity')]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('web_page', trans('admin.web_page'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::url('web_page', old('web_page'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_web_page'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('city_id', trans('admin.city'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('city_id', $cities, null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_city'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('address', trans('admin.address'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_address_company'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('password', trans('admin.password'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('admin.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('password_confirmation', trans('admin.password_confirmation'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('admin.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.insert_company'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/select2/js/select2.min.js') !!}
  {!! Html::script('plugins/rut/js/jquery.Rut.min.js') !!}
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $("#city_id").select2({
        placeholder: "{!! trans('admin.ph_city') !!}",
        allowClear: true
      });

      $('#rut').Rut({
        on_error: function(){ alert('Rut incorrecto'); },
        format_on: 'keyup'
      });
    });
  </script>
@endsection
