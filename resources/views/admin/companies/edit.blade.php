@extends('template.admin')

@section('title', trans('admin.edit_company') . ' ' . $company->name)

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['admin.admin.companies.update', $company], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('name', trans('admin.name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('name', $company->name, ['class' => 'form-control', 'placeholder' => trans('admin.ph_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('email', trans('admin.email'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::email('email', $company->email, ['class' => 'form-control', 'placeholder' => trans('admin.ph_email'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('activity', trans('admin.activity'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('activity', $company->activity, ['class' => 'form-control', 'placeholder' => trans('admin.ph_activity')]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('web_page', trans('admin.web_page'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('web_page', $company->web_page, ['class' => 'form-control', 'placeholder' => trans('admin.ph_web_page')]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('city_id', trans('admin.city'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('city_id', $cities, $company->city_id, ['class' => 'form-control', 'placeholder' => trans('admin.ph_city'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('address', trans('admin.address'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('address', $company->address, ['class' => 'form-control', 'placeholder' => trans('admin.ph_address_company'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.edit_company'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/select2/js/select2.min.js') !!}
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $("#city_id").select2({
        placeholder: "{!! trans('admin.ph_city') !!}",
        allowClear: true
      });
    });
  </script>
@endsection
