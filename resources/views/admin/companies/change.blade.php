@extends('template.admin')

@section('title', trans('admin.change_company') . ' a ' . $company->fullname)

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['admin.companies.change', $company->id], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('old_password', trans('admin.old_password'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::password('old_password', ['class' => 'form-control', 'placeholder' => trans('admin.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('new_password', trans('admin.new_password'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::password('new_password', ['class' => 'form-control', 'placeholder' => trans('admin.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('new_password_confirmation', trans('admin.new_password_confirmation'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::password('new_password_confirmation', ['class' => 'form-control', 'placeholder' => trans('admin.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.insert_company'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
