@extends('template.main')

@section('title', trans('admin.show_user') . ' ' . $user->fullname)

@section('content')
  <div>
    <ul>
      <li><b>{{ trans('admin.rut') }}:</b> {{ $user->rut }}</li>
      <li><b>{{ trans('admin.name') }}:</b> {{ $user->fullname }}</li>
      <li><b>{{ trans('admin.position') }}:</b> {{ $user->typeuser }}</li>
      <li><b>{{ trans('admin.email') }}:</b> {{ $user->email }}</li>
      <li><b>{{ trans('admin.telephone') }}:</b> {{ $user->telephone }}</li>
      <li><b>{{ trans('admin.city') }}:</b> {{ $user->city->name }}</li>
      <li><b>{{ trans('admin.address') }}:</b> {{ $user->address }}</li>
    </ul>
  </div>
@endsection
