@extends('template.admin')

@section('title', trans('admin.list_users'))

@section('content')
  <!-- Search -->
  {!! Form::model(Request::all(), ['route' => 'admin.users.index', 'method' => 'GET', 'class' => 'navbar-form']) !!}
    <div class='navbar-left'>
      <div class='input-group'>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('admin.search_user'), 'aria-describedby' => 'search']) !!}
        <span class="input-group-btn">
          {!! Form::button("<span class='glyphicon glyphicon-search' aria-hidden='true'>", array('class' => 'btn btn-search', 'type' => 'submit')) !!}
        </span>
      </div>
    </div>

    <div class='navbar-right'>
      <div class="input-group">
        <a href="{!! route('admin.users.create') !!}" class="btn btn-info">{!! trans('admin.insert_user') !!}</a>
      </div>
    </div>
  {!! Form::close() !!}
  <br>
  <hr>

  <!-- Content -->
  <div class="table-responsive">
    <table class="table table-hover">
      <thead>
        <th>{!! trans('admin.id') !!}</th>
        <th>{!! trans('admin.name') !!}</th>
        <th>{!! trans('admin.city') !!}</th>
        <th>{!! trans('admin.major') !!}</th>
        <th>{!! trans('admin.address') !!}</th>
        <th>{!! trans('admin.action') !!}</th>
      </thead>
      <tbody>
        @foreach($users as $user)
          <tr>
            <td>{!! $user->id !!}</td>
            <td>{!! $user->fullname !!}</td>
            <td>{!! $user->city->name !!}</td>
            <td>{!! $user->major->name !!}</td>
            <td>{!! $user->address !!}</td>
            <td>
              <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {!! trans('admin.action') !!} <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="{!! route('admin.users.show', $user->id) !!}">{!! trans('admin.show_users') !!}</a></li>
                  <li><a href="{!! route('admin.users.edit', $user->id) !!}">{!! trans('admin.edit_users') !!}</a></li>
                  <li><a href="{!! route('admin.users.destroy', $user->id) !!}">{!! trans('admin.destroy_users') !!}</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="{!! route('admin.users.page', $user->id ) !!}">{!! trans('admin.change_password') !!}</a></li>
                </ul>
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="text-center">
      {!! $users->appends(Request::all())->render() !!}
    </div>
  </div>
@endsection
