@extends('template.admin')

@section('title', trans('admin.insert_user'))

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => 'admin.users.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('rut', trans('admin.rut'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('rut', old('rut'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_rut'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('first_name', trans('admin.first_name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_first_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('last_name', trans('admin.last_name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_last_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('email', trans('admin.email'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_email'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('telephone', trans('admin.telephone'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('telephone', old('telephone'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_telephone')]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('major_id', trans('admin.major'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('major_id', $majors, null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_major'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('city_id', trans('admin.city'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('city_id', $cities, null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_city'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('address', trans('admin.address'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => trans('admin.ph_address_user'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('password', trans('admin.password'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('admin.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('password_confirmation', trans('admin.password_confirmation'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('admin.ph_password'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.insert_user'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/select2/js/select2.min.js') !!}
  {!! Html::script('plugins/rut/js/jquery.Rut.min.js') !!}
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $("#city_id").select2({
        placeholder: "{!! trans('admin.ph_city') !!}",
        allowClear: true
      });

      $("#area_id").select2({
        placeholder: "{!! trans('admin.ph_area') !!}",
        allowClear: true
      });

      $("#type").select2({
        placeholder: "{!! trans('admin.ph_type') !!}",
        allowClear: true
      });

      $('#rut').Rut({
        on_error: function(){ alert('Rut incorrecto'); },
        format_on: 'keyup'
      });
    });
  </script>
@endsection
