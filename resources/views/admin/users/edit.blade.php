@extends('template.admin')

@section('title', trans('admin.edit_user') . ' ' . $user->fullname)

@section('css')
  {!! Html::style('plugins/select2/css/select2.min.css') !!}
@endsection

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['admin.users.update', $user], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('first_name', trans('admin.name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('first_name', $user->first_name, ['class' => 'form-control', 'placeholder' => trans('admin.ph_first_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('last_name', trans('admin.last_name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('last_name', $user->last_name, ['class' => 'form-control', 'placeholder' => trans('admin.ph_last_name'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('email', trans('admin.email'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::email('email', $user->email, ['class' => 'form-control', 'placeholder' => trans('admin.ph_email'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('telephone', trans('admin.telephone'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('telephone', $user->telephone, ['class' => 'form-control', 'placeholder' => trans('admin.ph_telephone')]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('major_id', trans('admin.major'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('major_id', $majors, $user->major_id, ['class' => 'form-control', 'placeholder' => trans('admin.ph_major'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('city_id', trans('admin.city'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::select('city_id', $cities, $user->city_id, ['class' => 'form-control', 'placeholder' => trans('admin.ph_city'), 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label('address', trans('admin.address'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('address', $user->address, ['class' => 'form-control', 'placeholder' => trans('admin.ph_address_user'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.edit_user'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/select2/js/select2.min.js') !!}
  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $("#city_id").select2({
        placeholder: "{!! trans('admin.ph_city') !!}",
        allowClear: true
      });

      $("#major_id").select2({
        placeholder: "{!! trans('admin.ph_major') !!}",
        allowClear: true
      });
    });
  </script>
@endsection
