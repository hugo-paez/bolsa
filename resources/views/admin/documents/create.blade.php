@extends('template.main')

@section('title', trans('admin.insert_document'))

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['admin.documents.store', $event->id], 'method' => 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('file', trans('admin.upload_document'), ['class' => 'col-sm-2 control-label']) !!}
      <div class="col-sm-10">
        {!! Form::file('file[]', ['multiple', 'class' => 'multi', 'maxlength' => '10', 'minlength' => '1', 'accept' => 'pdf|rar|txt|doc|docx|xlsx|pptx|ppt|pps|ppsx|rtf', 'data-maxfile' => '6144']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.insert_document'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection

@section('javascript')
  {!! Html::script('plugins/multifile/js/jQuery.MultiFile.min.js') !!}
@endsection
