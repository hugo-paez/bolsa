@extends('template.admin')

@section('title', trans('admin.edit_region') . ' ' . $region->name)

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => ['admin.regions.update', $region], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('name', trans('admin.name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('name', $region->name, ['class' => 'form-control', 'placeholder' => trans('admin.ph_name_region'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.edit_region'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
