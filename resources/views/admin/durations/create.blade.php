@extends('template.admin')

@section('title', trans('admin.insert_duration'))

@section('content')
  <!-- Form -->
  {!! Form::open(['route' => 'admin.durations.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
    {!! csrf_field() !!}
    <div class="form-group">
      {!! Form::label('name', trans('admin.name'), ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('admin.ph_name_duration'), 'required']) !!}
      </div>
    </div>

    <div class="form-group text-center">
      {!! Form::submit(trans('admin.insert_duration'), ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection
