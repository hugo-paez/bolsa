<?php

namespace App\Http\Controllers\CompanyAuth;

use App\Company;
use App\City;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Register Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles the registration of new users as well as their
  | validation and creation. By default this controller uses a trait to
  | provide this functionality without requiring any additional code.
  |
  */

  use RegistersUsers;

  /**
   * Where to redirect users after login / registration.
   *
   * @var string
   */
  protected $redirectTo = '/company/home';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('company.guest');
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return Validator::make($data, [
      'rut'             =>  'min:11|max:12|required|unique:companies',
      'name'            =>  'min:4|max:150|required',
      'email'           =>  'min:4|max:255|required|email|unique:companies',
      'city_id'         =>  'required',
      'activity'        =>  'required',
      'address'         =>  'max:255|required',
      'web_page'        =>  'max:255|required|url',
      'password'	      =>	'required|min:6|confirmed',
    ]);
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return Company
   */
  protected function create(array $data)
  {
    $separator = ["-", "."];
    $data['rut'] = str_replace($separator, '', $data['rut']);

    return Company::create([
      'rut'             =>  $data['rut'],
      'name'            =>  $data['name'],
      'email'           =>  $data['email'],
      'city_id'         =>  $data['city_id'],
      'activity'        =>  $data['activity'],
      'address'         =>  $data['address'],
      'web_page'        =>  $data['web_page'],
      'password'	      =>	$data['password'],
    ]);
  }

  /**
   * Show the application registration form.
   *
   * @return \Illuminate\Http\Response
   */
  public function showRegistrationForm()
  {
    $cities = City::orderBy('name', 'ASC')->pluck('name', 'id');

    return view('company.auth.register', compact('cities'));
  }

  /**
   * Get the guard to be used during registration.
   *
   * @return \Illuminate\Contracts\Auth\StatefulGuard
   */
  protected function guard()
  {
    return Auth::guard('company');
  }
}
