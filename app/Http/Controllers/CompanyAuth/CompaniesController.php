<?php

namespace App\Http\Controllers\CompanyAuth;

use App\Company;
use App\City;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class CompaniesController extends Controller
{
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $cities = City::orderBy('name', 'ASC')->pluck('name', 'id');
    $company = Company::find($id);

    return view('company.edit', compact('company', 'cities'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $company = Company::find($id);
    $company->fill($request->all());
    $company->save();

    Flash::warning('¡Se ha modificado el usuario ' . $company->name . ' de forma exitosa!');
    return redirect()->route('offer.index');
  }

  public function page($id)
  {
    $company = Company::find($id);

    return view('company.change', compact('company'));
  }

  public function change($id, Request $request)
  {
    $company = Company::find($id);

    if(Hash::check($request->old_password, $company->password)){
      $company->password = $request->new_password;
      $company->save();

      Flash::success('¡Se ha cambiado la contraseña a ' . $company->name . ' de forma exitosa!');

      return redirect()->route('offer.index');
    } else {
      return redirect()->back();
    }
  }
}
