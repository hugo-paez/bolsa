<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Http\Requests\StoreOfferRequest;
use Illuminate\Support\Facades\Auth;
use App\Offer;
use App\Company;
use App\Duration;
use App\Workday;
use App\Experience;
use App\Position;
use App\Study;
use App\City;

class HomeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $offers = Offer::search($request->name)->orderBy('id', 'DESC')->paginate(10);
    $offers->each(function($offers){
      $offers->city;
      $offers->study;
      $offers->position;
      $offers->experience;
      $offers->workday;
      $offers->duration;
      $offers->company;
    });

    return view('index', compact('offers'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $durations = Duration::orderBy('name', 'ASC')->pluck('name', 'id');
    $positions = Position::orderBy('name', 'ASC')->pluck('name', 'id');
    $experiences = Experience::orderBy('name', 'ASC')->pluck('name', 'id');
    $studies = Study::orderBy('name', 'ASC')->pluck('name', 'id');
    $cities = City::orderBy('name', 'ASC')->pluck('name', 'id');
    $workdays = Workday::orderBy('name', 'ASC')->pluck('name', 'id');
    $companies = Company::orderBy('name', 'ASC')->pluck('name', 'id');

    return view('create', compact('durations', 'positions', 'experiences', 'studies', 'cities', 'workdays', 'companies'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreOfferRequest $request)
  {
    $offer = new Offer($request->all());
    $offer->company_id = Auth::guard('company')->user()->id;
    $offer->save();

    Flash::success('¡Se ha registrado ' . $offer->title . ' de forma exitosa!');
    return redirect()->route('offer.index');
  }
}
