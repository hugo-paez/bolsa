<?php

namespace App\Http\Controllers\UserAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use Illuminate\Http\Request;

class LoginController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers, LogsoutGuard {
    LogsoutGuard::logout insteadof AuthenticatesUsers;
  }

  /**
   * Where to redirect users after login / registration.
   *
   * @var string
   */
  public $redirectTo = '';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('user.guest', ['except' => 'logout']);
  }

  /**
   * Show the application's login form.
   *
   * @return \Illuminate\Http\Response
   */
  public function showLoginForm()
  {
    return view('user.auth.login');
  }

  /**
   * Get the guard to be used during authentication.
   *
   * @return \Illuminate\Contracts\Auth\StatefulGuard
   */
  protected function guard()
  {
    return Auth::guard('user');
  }

  /**
   * Get the login username to be used by the controller.
   *
   * @return string
   */
  public function username()
  {
      return 'rut';
  }

  /**
   * Get the needed authorization credentials from the request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  protected function credentials(Request $request)
  {
    $array = $request->only($this->username(), 'password');
    $separator = ["-", "."];
    $array['rut'] = str_replace($separator, '', $array['rut']);

    return $array;
  }

  /**
   * Handle an authentication attempt.
   *
   * @return Response
   */
  public function authenticate()
  {
    if (Auth::attempt(array('rut' => $rut, 'password' => $password), true)) {
      return redirect()->intended('home');
    }
  }
}
