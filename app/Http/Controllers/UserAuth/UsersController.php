<?php

namespace App\Http\Controllers\UserAuth;

use App\User;
use App\City;
use App\Major;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class UsersController extends Controller
{
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $cities = City::orderBy('name', 'ASC')->pluck('name', 'id');
    $majors = Major::orderBy('name', 'ASC')->pluck('name', 'id');
    $user = User::find($id);

    return view('user.edit', compact('user', 'cities', 'majors'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $user = User::find($id);
    $user->fill($request->all());
    $user->save();

    Flash::warning('¡Se ha modificado el usuario ' . $user->fullname . ' de forma exitosa!');

    return redirect()->route('offer.index');
  }

  public function page($id)
  {
    $user = User::find($id);

    return view('user.change', compact('user'));
  }

  public function change($id, Request $request)
  {
    $user = User::find($id);

    if(Hash::check($request->old_password, $user->password)){
      $user->password = $request->new_password;
      $user->save();

      Flash::success('¡Se ha cambiado la contraseña a ' . $user->fullname . ' de forma exitosa!');

      return redirect()->route('offer.index');
    } else {
      return redirect()->back();
    }
  }
}
