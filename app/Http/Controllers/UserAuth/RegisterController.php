<?php

namespace App\Http\Controllers\UserAuth;

use App\User;
use App\City;
use App\Major;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Register Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles the registration of new users as well as their
  | validation and creation. By default this controller uses a trait to
  | provide this functionality without requiring any additional code.
  |
  */

  use RegistersUsers;

  /**
   * Where to redirect users after login / registration.
   *
   * @var string
   */
  protected $redirectTo = '/user/home';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('user.guest');
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return Validator::make($data, [
      'rut'             =>  'min:11|max:12|required|unique:users',
      'first_name'      =>  'min:4|max:50|required',
      'last_name'       =>  'min:4|max:50|required',
      'email'           =>  'min:4|max:255|required|email|unique:users',
      'city_id'         =>  'required',
      'major_id'        =>  'required',
      'address'         =>  'max:255|required',
      'telephone'       =>  'max:12|required',
      'password'	      =>	'required|min:6|confirmed',
    ]);
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return User
   */
  protected function create(array $data)
  {
    $separator = ["-", "."];
    $data['rut'] = str_replace($separator, '', $data['rut']);

    return User::create([
      'rut'           =>  $data['rut'],
      'first_name'    =>  $data['first_name'],
      'last_name'     =>  $data['last_name'],
      'email'         =>  $data['email'],
      'city_id'       =>  $data['city_id'],
      'major_id'      =>  $data['major_id'],
      'address'       =>  $data['address'],
      'telephone'     =>  $data['telephone'],
      'password'      =>  $data['password'],
    ]);
  }

  /**
   * Show the application registration form.
   *
   * @return \Illuminate\Http\Response
   */
  public function showRegistrationForm()
  {
    $cities = City::orderBy('name', 'ASC')->pluck('name', 'id');
    $majors = Major::orderBy('name', 'ASC')->pluck('name', 'id');

    return view('user.auth.register', compact('cities', 'majors'));
  }

  /**
   * Get the guard to be used during registration.
   *
   * @return \Illuminate\Contracts\Auth\StatefulGuard
   */
  protected function guard()
  {
    return Auth::guard('user');
  }
}
