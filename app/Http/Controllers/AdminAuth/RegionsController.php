<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRegionRequest;
use App\Http\Requests\Admin\UpdateRegionRequest;
use Laracasts\Flash\Flash;
use App\Region;

class RegionsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $regions = Region::search($request->name)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.regions.index', compact('regions'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.regions.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreRegionRequest $request)
  {
    $region = new Region($request->all());
    $region->save();

    Flash::success('¡Se ha registrado ' . $region->name . ' de forma exitosa!');

    return redirect()->route('admin.regions.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $region = Region::find($id);

    return view('admin.regions.edit', compact('region'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateRegionRequest $request, $id)
  {
    $region = Region::find($id);
    $region->fill($request->all());
    $region->save();

    Flash::warning('¡Se ha modificado la región ' . $region->name . ' de forma exitosa!');

    return redirect()->route('admin.regions.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $region = Region::find($id);
    $region->delete();

    Flash::error('¡Se ha eliminado la región ' . $region->name . ' de forma exitosa!');

    return redirect()->route('admin.regions.index');
  }
}
