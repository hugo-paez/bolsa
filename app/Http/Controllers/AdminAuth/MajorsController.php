<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreMajorRequest;
use App\Http\Requests\Admin\UpdateMajorRequest;
use Laracasts\Flash\Flash;
use App\Major;

class MajorsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $majors = Major::search($request->name)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.majors.index', compact('majors'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.majors.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreMajorRequest $request)
  {
    $major = new Major($request->all());
    $major->save();

    Flash::success('¡Se ha registrado ' . $major->name . ' de forma exitosa!');

    return redirect()->route('admin.majors.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $major = Major::find($id);

    return view('admin.majors.edit', compact('major'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateMajorRequest $request, $id)
  {
    $major = Major::find($id);
    $major->fill($request->all());
    $major->save();

    Flash::warning('¡Se ha modificado la región ' . $major->name . ' de forma exitosa!');

    return redirect()->route('admin.majors.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $major = Major::find($id);
    $major->delete();

    Flash::error('¡Se ha eliminado la región ' . $major->name . ' de forma exitosa!');

    return redirect()->route('admin.majors.index');
  }
}
