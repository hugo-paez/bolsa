<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCityRequest;
use App\Http\Requests\Admin\UpdateCityRequest;
use Laracasts\Flash\Flash;
use App\City;
use App\Region;

class CitiesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $cities = City::search($request->name)->orderBy('id', 'DESC')->paginate(10);
    $cities->each(function($cities){
      $cities->region;
    });

    return view('admin.cities.index', compact('cities'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $regions = Region::orderBy('name', 'ASC')->pluck('name', 'id');

    return view('admin.cities.create', compact('regions'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreCityRequest $request)
  {
    $city = new City($request->all());
    $city->save();

    Flash::success('¡Se ha registrado ' . $city->name . ' de forma exitosa!');

    return redirect()->route('admin.cities.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $regions = Region::orderBy('name', 'ASC')->pluck('name', 'id');
    $city = City::find($id);

    return view('admin.cities.edit', compact('city','regions'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateCityRequest $request, $id)
  {
    $city = City::find($id);
    $city->fill($request->all());
    $city->save();

    Flash::warning('¡Se ha modificado la ciudad ' . $city->name . ' de forma exitosa!');

    return redirect()->route('admin.cities.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $city = City::find($id);
    $city->delete();

    Flash::error('¡Se ha eliminado la ciudad ' . $city->name . ' de forma exitosa!');

    return redirect()->route('admin.cities.index');
  }
}
