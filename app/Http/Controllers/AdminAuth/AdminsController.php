<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAdminRequest;
use App\Http\Requests\Admin\UpdateAdminRequest;
use App\Http\Requests\Admin\ChangeAdminRequest;
use Laracasts\Flash\Flash;
use App\Admin;
use Hash;

class AdminsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $admins = Admin::search($request->name)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.admins.index', compact('admins'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.admins.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreAdminRequest $request)
  {
    $admin = new Admin($request->all());
    $admin->save();

    Flash::success('¡Se ha registrado ' . $admin->fullname . ' de forma exitosa!');

    return redirect()->route('admin.admins.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $admin = Admin::find($id);

    return view('admin.admins.edit', compact('admin'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateAdminRequest $request, $id)
  {
    $admin = Admin::find($id);
    $admin->fill($request->all());
    $admin->save();

    Flash::warning('¡Se ha modificado la región ' . $admin->fullname . ' de forma exitosa!');

    return redirect()->route('admin.admins.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $admin = Admin::find($id);
    $admin->delete();

    Flash::error('¡Se ha eliminado la región ' . $admin->fullname . ' de forma exitosa!');

    return redirect()->route('admin.admins.index');
  }

  public function page($id)
  {
    $admin = Admin::find($id);

    return view('admin.admins.change', compact('admin'));
  }

  public function change($id, ChangeAdminRequest $request)
  {
    $admin = Admin::find($id);

    if(Hash::check($request->old_password, $admin->password)){
      $admin->password = $request->new_password;
      $admin->save();

      Flash::success('¡Se ha cambiado la contraseña a ' . $admin->fullname . ' de forma exitosa!');

      return redirect()->route('admin.admins.index');
    } else {
      return redirect()->back();
    }
  }
}
