<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreWorkdayRequest;
use App\Http\Requests\Admin\UpdateWorkdayRequest;
use Laracasts\Flash\Flash;
use App\Workday;

class WorkdaysController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $workdays = Workday::search($request->name)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.workdays.index', compact('workdays'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.workdays.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreWorkdayRequest $request)
  {
    $workday = new Workday($request->all());
    $workday->save();

    Flash::success('¡Se ha registrado ' . $workday->name . ' de forma exitosa!');

    return redirect()->route('admin.workdays.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $workday = Workday::find($id);

    return view('admin.workdays.edit', compact('workday'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateWorkdayRequest $request, $id)
  {
    $workday = Workday::find($id);
    $workday->fill($request->all());
    $workday->save();

    Flash::warning('¡Se ha modificado la región ' . $workday->name . ' de forma exitosa!');

    return redirect()->route('admin.workdays.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $workday = Workday::find($id);
    $workday->delete();

    Flash::error('¡Se ha eliminado la región ' . $workday->name . ' de forma exitosa!');

    return redirect()->route('admin.workdays.index');
  }
}
