<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUserRequest;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Http\Requests\Admin\ChangeUserRequest;
use Laracasts\Flash\Flash;
use App\User;
use App\City;
use App\Major;

class UsersController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $users = User::searchName($request->name)->orderBy('id', 'DESC')->paginate(10);
    $users->each(function($users){
      $users->city;
      $users->major;
    });

    return view('admin.users.index', compact('users'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $cities = City::orderBy('name', 'ASC')->pluck('name', 'id');
    $majors = Major::orderBy('name', 'ASC')->pluck('name', 'id');

    return view('admin.users.create', compact('cities', 'majors'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreUserRequest $request)
  {
    $user = new User($request->all());
    $user->save();

    Flash::success('¡Se ha registrado ' . $user->fullname . ' de forma exitosa!');
    return redirect()->route('admin.users.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $user = User::findOrFail($id);

    return view('admin.users.show', compact('user'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $cities = City::orderBy('name', 'ASC')->pluck('name', 'id');
    $majors = Major::orderBy('name', 'ASC')->pluck('name', 'id');
    $user = User::find($id);

    return view('admin.users.edit', compact('user', 'cities', 'majors'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateUserRequest $request, $id)
  {
    $user = User::find($id);
    $user->fill($request->all());
    $user->save();

    Flash::warning('¡Se ha modificado el usuario ' . $user->fullname . ' de forma exitosa!');
    return redirect()->route('admin.users.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $user = User::find($id);
    $user->delete();

    Flash::error('¡Se ha eliminado el usuario ' . $user->fullname . ' de forma exitosa!');
    return redirect()->route('admin.users.index');
  }

  public function page($id)
  {
    $user = User::find($id);

    return view('admin.users.change', compact('user'));
  }

  public function change($id, ChangeUserRequest $request)
  {
    $user = User::find($id);

    if(Hash::check($request->old_password, $user->password)){
      $user->password = $request->new_password;
      $user->save();

      Flash::success('¡Se ha cambiado la contraseña a ' . $user->fullname . ' de forma exitosa!');

      return redirect()->route('admin.users.index');
    } else {
      return redirect()->back();
    }
  }
}
