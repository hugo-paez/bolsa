<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreOfferRequest;
use App\Http\Requests\Admin\UpdateOfferRequest;
use Laracasts\Flash\Flash;
use App\Offer;
use App\Company;
use App\Duration;
use App\Workday;
use App\Experience;
use App\Position;
use App\Study;
use App\City;

class OffersController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $offers = Offer::search($request->name)->orderBy('id', 'DESC')->paginate(10);
    $offers->each(function($offers){
      $offers->city;
      $offers->study;
      $offers->position;
      $offers->experience;
      $offers->workday;
      $offers->duration;
      $offers->company;
    });

    return view('admin.offers.index', compact('offers'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $durations = Duration::orderBy('name', 'ASC')->pluck('name', 'id');
    $positions = Position::orderBy('name', 'ASC')->pluck('name', 'id');
    $experiences = Experience::orderBy('name', 'ASC')->pluck('name', 'id');
    $studies = Study::orderBy('name', 'ASC')->pluck('name', 'id');
    $cities = City::orderBy('name', 'ASC')->pluck('name', 'id');
    $workdays = Workday::orderBy('name', 'ASC')->pluck('name', 'id');
    $companies = Company::orderBy('name', 'ASC')->pluck('name', 'id');

    return view('admin.offers.create', compact('durations', 'positions', 'experiences', 'studies', 'cities', 'workdays', 'companies'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreOfferRequest $request)
  {
    $offer = new Offer($request->all());
    $offer->save();

    Flash::success('¡Se ha registrado ' . $offer->title . ' de forma exitosa!');
    return redirect()->route('admin.offers.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $durations = Duration::orderBy('name', 'ASC')->pluck('name', 'id');
    $positions = Position::orderBy('name', 'ASC')->pluck('name', 'id');
    $experiences = Experience::orderBy('name', 'ASC')->pluck('name', 'id');
    $studies = Study::orderBy('name', 'ASC')->pluck('name', 'id');
    $cities = City::orderBy('name', 'ASC')->pluck('name', 'id');
    $workday = Workday::orderBy('name', 'ASC')->pluck('name', 'id');
    $companies = Company::orderBy('name', 'ASC')->pluck('name', 'id');
    $offer = Offer::find($id);

    return view('admin.offers.edit', compact('offer', 'positions', 'experiences', 'studies', 'cities', 'workday', 'companies'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateOfferRequest $request, $id)
  {
    $offer = Offer::find($id);
    $offer->fill($request->all());
    $offer->save();

    Flash::warning('¡Se ha modificado el usuario ' . $offer->title . ' de forma exitosa!');
    return redirect()->route('admin.offers.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $offer = Offer::find($id);
    $offer->delete();

    Flash::error('¡Se ha eliminado el usuario ' . $offer->title . ' de forma exitosa!');
    return redirect()->route('admin.offers.index');
  }
}
