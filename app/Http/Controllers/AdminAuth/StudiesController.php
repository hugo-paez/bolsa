<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreStudyRequest;
use App\Http\Requests\Admin\UpdateStudyRequest;
use Laracasts\Flash\Flash;
use App\Study;

class StudiesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $studies = Study::search($request->name)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.studies.index', compact('studies'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.studies.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreStudyRequest $request)
  {
    $study = new Study($request->all());
    $study->save();

    Flash::success('¡Se ha registrado ' . $study->name . ' de forma exitosa!');

    return redirect()->route('admin.studies.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $study = Study::find($id);

    return view('admin.studies.edit', compact('study'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateStudyRequest $request, $id)
  {
    $study = new Study($request->all());
    $study->save();

    Flash::success('¡Se ha registrado ' . $study->name . ' de forma exitosa!');

    return redirect()->route('admin.studies.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $study = Study::find($id);
    $study->delete();

    Flash::error('¡Se ha eliminado la región ' . $study->name . ' de forma exitosa!');

    return redirect()->route('admin.studies.index');
  }
}
