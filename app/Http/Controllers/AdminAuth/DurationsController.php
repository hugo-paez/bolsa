<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use App\Http\Requests\Admin\StoreDurationRequest;
use App\Http\Requests\Admin\UpdateDurationRequest;
use App\Duration;

class DurationsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
   $durations = Duration::search($request->name)->orderBy('id', 'DESC')->paginate(10);

   return view('admin.durations.index', compact('durations'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.durations.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreDurationRequest $request)
  {
    $daration = new Duration($request->all());
    $daration->save();

    Flash::success('¡Se ha registrado ' . $daration->name . ' de forma exitosa!');

    return redirect()->route('admin.durations.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $daration = Duration::find($id);

    return view('admin.durations.edit', compact('duration'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateDurationRequest $request, $id)
  {
    $daration = Duration::find($id);
    $daration->fill($request->all());
    $daration->save();

    Flash::warning('¡Se ha modificado la región ' . $daration->name . ' de forma exitosa!');

    return redirect()->route('admin.durations.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $daration = Duration::find($id);
    $daration->delete();

    Flash::error('¡Se ha eliminado la región ' . $daration->name . ' de forma exitosa!');

    return redirect()->route('admin.durations.index');
  }
}
