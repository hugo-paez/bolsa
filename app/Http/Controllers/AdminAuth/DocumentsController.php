<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreDocumentRequest;
use App\Http\Requests\Admin\UpdateDocumentRequest;
use Laracasts\Flash\Flash;
use App\Document;

class DocumentsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $documents = Document::search($request->name)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.documents.index', compact('documents'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.documents.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreDocumentRequest $request)
  {
    $document = new Document($request->all());
    $document->save();

    Flash::success('¡Se ha registrado ' . $document->name . ' de forma exitosa!');

    return redirect()->route('admin.documents.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $experience = Document::find($id);

    return view('admin.documents.edit', compact('document'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateDocumentRequest $request, $id)
  {
    $document = Document::find($id);
    $document->fill($request->all());
    $document->save();

    Flash::warning('¡Se ha modificado la región ' . $document->name . ' de forma exitosa!');

    return redirect()->route('admin.documents.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $document = Document::find($id);
    $document->delete();

    Flash::error('¡Se ha eliminado la región ' . $document->name . ' de forma exitosa!');

    return redirect()->route('admin.documents.index');
  }
}
