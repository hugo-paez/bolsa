<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StorePositionRequest;
use App\Http\Requests\Admin\UpdatePositionRequest;
use Laracasts\Flash\Flash;
use App\Position;

class PositionsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $positions = Position::search($request->name)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.positions.index', compact('positions'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.positions.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StorePositionRequest $request)
  {
    $position = new Position($request->all());
    $position->save();

    Flash::success('¡Se ha registrado ' . $position->name . ' de forma exitosa!');

    return redirect()->route('admin.positions.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $position = Position::find($id);

    return view('admin.positions.edit', compact('position'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdatePositionRequest $request, $id)
  {
    $position = Position::find($id);
    $position->fill($request->all());
    $position->save();

    Flash::warning('¡Se ha modificado la región ' . $position->name . ' de forma exitosa!');

    return redirect()->route('admin.positions.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $position = Position::find($id);
    $position->delete();

    Flash::error('¡Se ha eliminado la región ' . $position->name . ' de forma exitosa!');

    return redirect()->route('admin.positions.index');
  }
}
