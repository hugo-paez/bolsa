<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCompanyRequest;
use App\Http\Requests\Admin\UpdateCompanyRequest;
use App\Http\Requests\Admin\ChangeCompanyRequest;
use Laracasts\Flash\Flash;
use App\Company;
use App\City;

class CompaniesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $companies = Company::search($request->name)->orderBy('id', 'DESC')->paginate(10);
    $companies->each(function($companies){
      $companies->city;
    });

    return view('admin.companies.index', compact('companies'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $cities = City::orderBy('name', 'ASC')->pluck('name', 'id');

    return view('admin.companies.create', compact('cities'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreCompanyRequest $request)
  {
    $company = new Company($request->all());
    $company->save();

    Flash::success('¡Se ha registrado ' . $company->name . ' de forma exitosa!');
    return redirect()->route('admin.companies.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $company = Company::findOrFail($id);

    return view('admin.companies.show', compact('company'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $cities = City::orderBy('name', 'ASC')->pluck('name', 'id');
    $company = Company::find($id);

    return view('admin.companies.edit', compact('company', 'cities'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateCompanyRequest $request, $id)
  {
    $company = Company::find($id);
    $company->fill($request->all());
    $company->save();

    Flash::warning('¡Se ha modificado el usuario ' . $company->name . ' de forma exitosa!');
    return redirect()->route('admin.companies.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $company = Company::find($id);
    $company->delete();

    Flash::error('¡Se ha eliminado el usuario ' . $company->name . ' de forma exitosa!');
    return redirect()->route('admin.companies.index');
  }

  public function page($id)
  {
    $company = Company::find($id);

    return view('admin.companies.change', compact('company'));
  }

  public function change($id, ChangeCompanyRequest $request)
  {
    $company = Company::find($id);

    if(Hash::check($request->old_password, $company->password)){
      $company->password = $request->new_password;
      $company->save();

      Flash::success('¡Se ha cambiado la contraseña a ' . $company->name . ' de forma exitosa!');

      return redirect()->route('admin.companies.index');
    } else {
      return redirect()->back();
    }
  }
}
