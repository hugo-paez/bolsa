<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use App\Http\Requests\Admin\StoreExperienceRequest;
use App\Http\Requests\Admin\UpdateExperienceRequest;
use App\Experience;

class ExperiencesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $experiences = Experience::search($request->name)->orderBy('id', 'DESC')->paginate(10);

    return view('admin.experiences.index', compact('experiences'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.experiences.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreExperienceRequest $request)
  {
    $experience = new Experience($request->all());
    $experience->save();

    Flash::success('¡Se ha registrado ' . $experience->name . ' de forma exitosa!');

    return redirect()->route('admin.experiences.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $experience = Experience::find($id);

    return view('admin.experiences.edit', compact('experience'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateExperienceRequest $request, $id)
  {
    $experience = Experience::find($id);
    $experience->fill($request->all());
    $experience->save();

    Flash::warning('¡Se ha modificado la región ' . $experience->name . ' de forma exitosa!');

    return redirect()->route('admin.experiences.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $experience = Experience::find($id);
    $experience->delete();

    Flash::error('¡Se ha eliminado la región ' . $experience->name . ' de forma exitosa!');

    return redirect()->route('admin.experiences.index');
  }
}
