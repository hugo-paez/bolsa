<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'first_name'                =>  'min:4|max:50|required|string',
      'last_name'                 =>  'min:4|max:50|required|string',
      'email'                     =>  'min:4|max:255|required|email',
      'city_id'                   =>  'required',
      'major_id'                  =>  'required',
      'type'                      =>  'required',
      'address'                   =>  'max:255|required',
      'telephone'                 =>  'max:12|required',
    ];
  }
}
