<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreOfferRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'title'             =>  'min:4|max:255|required',
      'description'       =>  'min:4|max:255|required',
      'requirement'       =>  'min:4|max:255|required',
      'vacancy'           =>  'digits_between:1,3',
      'company_id'        =>  'required',
      'duration_id'       =>  'required',
      'workday_id'        =>  'required',
      'experience_id'     =>  'required',
      'position_id'       =>  'required',
      'study_id'          =>  'required',
      'position_id'       =>  'required',
      'city_id'           =>  'required',
    ];
  }
}
