<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdminRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'first_name'                =>  'min:4|max:50|required|string',
      'last_name'                 =>  'min:4|max:50|required|string',
      'email'                     =>  'min:4|max:255|required|email|unique:users',
      'password'                  =>  'min:6|max:60|required|confirmed',
      'password_confirmation'	    =>  'min:6|max:60|required',
    ];
  }
}
