<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreCompanyRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'rut'                       =>  'min:12|max:13|required|unique:users',
      'name'                      =>  'min:4|max:60|required',
      'email'                     =>  'min:4|max:255|required|email',
      'activity'                  =>  'max:255|required',
      'web_page'                  =>  'url',
      'address'                   =>  'max:255|required',
      'city_id'                   =>  'required',
      'password'                  =>  'min:6|max:60|required|confirmed',
      'password_confirmation'	    =>  'min:6|max:60|required',
    ];
  }
}
