<?php

namespace App;

use App\Notifications\UserResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'rut', 'first_name', 'last_name', 'email', 'photo', 'telephone', 'address', 'password', 'major_id', 'city_id'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
    $this->notify(new UserResetPassword($token));
  }

  public function city()
  {
    return $this->belongsTo(City::class);
  }

  public function major()
  {
    return $this->belongsTo(City::class);
  }

  public function scopeSearchRut($query, $rut)
  {
    return $query->where('rut', 'LIKE', "%$rut%");
  }

  public function scopeSearchName($query, $name)
  {
    $name = str_replace(' ', '%', $name);
    return $query->where(DB::raw("CONCAT(first_name, ' ', last_name)"), 'LIKE', "%$name%");
  }

  public function setPasswordAttribute($password)
  {
    $this->attributes['password'] = bcrypt($password);
  }

  public function setFirstNameAttribute($first_name)
  {
    $this->attributes['first_name'] = ucwords($first_name);
  }

  public function setLastNameAttribute($last_name)
  {
    $this->attributes['last_name'] = ucwords($last_name);
  }

  public function setAddressAttribute($address)
  {
    $this->attributes['address'] = ucwords($address);
  }

  public function getFullNameAttribute()
  {
    if(strstr($this->attributes['first_name'], ' ', strlen($this->attributes['first_name'])) != null)
    {
      $first_name = strstr($this->attributes['first_name'], ' ', strlen($this->attributes['first_name']));
    }
    else
    {
      $first_name = $this->attributes['first_name'];
    }

    if(strstr($this->attributes['last_name'], ' ', strlen($this->attributes['last_name'])) != null)
    {
      $last_name = strstr($this->attributes['last_name'], ' ', strlen($this->attributes['last_name']));
    }
    else
    {
      $last_name = $this->attributes['last_name'];
    }
    return $first_name . ' ' . $last_name;
  }

  public function setRutAttribute($rut)
  {
    $separator = ["-", "."];
    $rut = str_replace($separator, '', $rut);

    $this->attributes['rut'] = $rut;
  }
}
