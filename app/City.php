<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cities';

  protected $fillable = [
    'name', 'region_id'
  ];

  public function region()
  {
    return $this->belongsTo(Region::class);
  }

  public function users()
  {
    return $this->hasMany(User::class);
  }

  public function companies()
  {
    return $this->hasMany(Company::class);
  }

  public function scopeSearch($query, $name)
  {
    return $query->where('name', 'LIKE', "%$name%");
  }
}
