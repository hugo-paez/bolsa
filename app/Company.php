<?php

namespace App;

use App\Notifications\CompanyResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Company extends Authenticatable
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'rut', 'name', 'email', 'activity', 'web_page', 'photo', 'address' ,'password', 'city_id'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
    $this->notify(new CompanyResetPassword($token));
  }

  public function city()
  {
    return $this->belongsTo(City::class);
  }

  public function scopeSearch($query, $name)
  {
    return $query->where('name', 'LIKE', "%$name%");
  }

  public function setPasswordAttribute($password)
  {
    $this->attributes['password'] = bcrypt($password);
  }

  public function setAddressAttribute($address)
  {
    $this->attributes['address'] = ucwords($address);
  }

  public function setRutAttribute($rut)
  {
    $separator = ["-", "."];
    $rut = str_replace($separator, '', $rut);

    $this->attributes['rut'] = $rut;
  }
}
