<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'positions';

  protected $fillable = [
    'name'
  ];

  public function offers()
  {
    return $this->hasMany(Offer::class);
  }

  public function scopeSearch($query, $name)
  {
    return $query->where('name', 'LIKE', "%$name%");
  }
}
