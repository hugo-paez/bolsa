<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Duration extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'durations';

  protected $fillable = [
    'name'
  ];

  public function offers()
  {
    return $this->belongsToMany(Offer::class)->withTimestamps();
  }

  public function scopeSearch($query, $name)
  {
    return $query->where('name', 'LIKE', "%$name%");
  }
}
