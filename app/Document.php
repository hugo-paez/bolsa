<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'documents';

  protected $fillable = [
    'name'
  ];

  public function user()
  {
    return $this->hasOne(User::class);
  }

  public function scopeSearch($query, $name)
  {
    return $query->where('name', 'LIKE', "%$name%");
  }
}
