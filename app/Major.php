<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'majors';

  protected $fillable = [
    'name'
  ];

  public function offers()
  {
    return $this->belongsToMany(Offer::class)->withTimestamps();
  }

  public function users()
  {
    return $this->hasMany(User::class);
  }

  public function scopeSearch($query, $name)
  {
    return $query->where('name', 'LIKE', "%$name%");
  }
}
