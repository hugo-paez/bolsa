<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'offers';

  protected $fillable = [
    'title', 'description', 'date', 'requirement', 'vacancy', 'position', 'status', 'company_id', 'duration_id', 'workday_id', 'experience_id', 'position_id', 'study_id', 'city_id'
  ];

  public function company()
  {
    return $this->belongsTo(Company::class);
  }

  public function duration()
  {
    return $this->belongsTo(Duration::class);
  }

  public function experience()
  {
    return $this->belongsTo(Experience::class);
  }

  public function position()
  {
    return $this->belongsTo(Position::class);
  }

  public function study()
  {
    return $this->belongsTo(Study::class);
  }

  public function workday()
  {
    return $this->belongsTo(Workday::class);
  }

  public function city()
  {
    return $this->belongsTo(City::class);
  }

  public function majors()
  {
    return $this->belongsToMany(Major::class)->withTimestamps();
  }

  public function scopeSearch($query, $title)
  {
    return $query->where('title', 'LIKE', "%$title%");
  }
}
